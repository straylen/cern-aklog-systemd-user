# cern-aklog-systemd-user

* cern-aklog-systemd-user is a simple RPM which packages a systemd user
unit file for use when running with AFS on systemd v239+ (CentOS8)
* This is a workaround to provide AFS PAGs through the per-user systemd
instance
* More details on why this workaround exists can be found here:
https://github.com/systemd/systemd/issues/7261
