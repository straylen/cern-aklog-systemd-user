Name:		cern-aklog-systemd-user
Version:	1.0
Release:	1%{?dist}
Summary:	cern-aklog-systemd-user	

Group:		CERN/Utilities
License:	BSD	
URL:		http://linux.cern.ch
Source0:        %{name}-%{version}.tgz
BuildArch:	noarch

Requires:	openafs-krb5
Requires:	kstart

%description
cern-aklog-systemd-user is a simple RPM which packages a systemd user
unit file for use when running with AFS on systemd v239+ (CentOS8)
This is a workaround to provide AFS PAGs through the per-user systemd
instance.
More details on why this workaround exists can be found here:
https://github.com/systemd/systemd/issues/7261

%prep
%setup -q

%build

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_userunitdir}
install -p -m 0644 aklog.service %{buildroot}/%{_userunitdir}/aklog.service

%files
/%{_userunitdir}/aklog.service

%post
%systemd_user_post aklog.service

%preun
%systemd_user_preun aklog.service

%postun
%systemd_user_postun_with_restart aklog.service

%changelog
* Fri Jan 10 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
 - initial release
